import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Configuration } from 'src/configuration';
import { Building } from './building.model';

@Injectable({
  providedIn: 'root'
})
export class BuildingsService {

  constructor(private _http: HttpClient) { }

  getBuildings() {
    return this._http.get<Array<Building>>(`${Configuration.REMOTE_API_URL}places/`)
  }

}
