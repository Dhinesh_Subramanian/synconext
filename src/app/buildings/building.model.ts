export interface Building {
    "id": number,
    "building": string,
    "city": string,
    "state": string,
    "lat": string,
    "long": string,
    "phon": string,
    "email": string,
    "rating": number
}