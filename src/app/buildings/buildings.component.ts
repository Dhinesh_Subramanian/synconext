import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { Building } from './building.model';
import { BuildingsService } from './buildings.service';

@Component({
  selector: 'app-buildings',
  templateUrl: './buildings.component.html',
  styleUrls: ['./buildings.component.css']
})
export class BuildingsComponent implements OnInit {

  buildingsInfo: Array<Building> = []
  tableData: Array<Building> = []
  buildings: Array<string> = []
  cities: Array<string> = []
  states: Array<string> = []
  curPage: number = 1;
  pageSize: number = 10;
  searchForm: FormGroup = new FormGroup({
    building: new FormControl('', this.checkForValidInput(SEARCH_CATEGORY.BUILDING)),
    city: new FormControl('', this.checkForValidInput(SEARCH_CATEGORY.CITY)),
    state: new FormControl('', this.checkForValidInput(SEARCH_CATEGORY.STATE))
  })
  searchByName: SEARCH_CATEGORY = SEARCH_CATEGORY.BUILDING
  searchByCity: SEARCH_CATEGORY = SEARCH_CATEGORY.CITY
  searchByState: SEARCH_CATEGORY = SEARCH_CATEGORY.STATE
  get name() { return this.searchForm.controls['building'] }
  get city() { return this.searchForm.controls['city'] }
  get state() { return this.searchForm.controls['state'] }
  loading: boolean = false
  maxRating: number = 5
  sort: string = 'asc'

  constructor(private _buildings: BuildingsService) { }

  ngOnInit(): void {
    localStorage.getItem('sort') ? this.sort = localStorage.getItem('sort') as string : ''
    this.getBuildingsInfo()
  }

  checkForValidInput(category: SEARCH_CATEGORY): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => this.getInfoByCategory(category).includes(control.value.trim())
      ? null : { invalidInput: control.value };
  }

  getBuildingsInfo() {
    this.loading = true
    if (localStorage.getItem('places')) {
      this.loading = false
      this.buildingsInfo = JSON.parse(localStorage.getItem('places') as string)
      this.fillTable()
      this.sortData()
    } else {
      this._buildings.getBuildings().subscribe((res: Array<Building>) => {
        this.loading = false
        this.buildingsInfo = this.giveRatingFacility(res);
        localStorage.setItem('places', JSON.stringify(this.buildingsInfo))
        this.fillTable()
        this.sortData()
      }, err => this.loading = false)
    }
  }

  giveRatingFacility(buildings: Array<Building>) {
    return buildings.map(building => { building.rating = 0; return building })
  }

  fillTable() {
    this.tableData = this.buildingsInfo
    this.fillSearchFieldOptions()
  }

  getInfoByCategory(category: SEARCH_CATEGORY) {
    return [...new Set(this.buildingsInfo.map(info => info[category]))]
  }

  numberOfPages() {
    return Math.ceil(this.tableData.length / this.pageSize);
  };

  pickBuilding(event: Event, category: SEARCH_CATEGORY) {
    if ((<HTMLInputElement>event.target).value.trim()) {
      switch (category) {
        case SEARCH_CATEGORY.BUILDING:
          this.cities = this.buildingsInfo.filter(building => building.building === (<HTMLInputElement>event.target).value).map(building => building.city)
          this.states = this.buildingsInfo.filter(building => building.building === (<HTMLInputElement>event.target).value).map(building => building.state)
          break;
        case SEARCH_CATEGORY.CITY:
          this.buildings = this.buildingsInfo.filter(building => building.city === (<HTMLInputElement>event.target).value).map(building => building.building)
          this.states = this.buildingsInfo.filter(building => building.city === (<HTMLInputElement>event.target).value).map(building => building.state)
          break;
        case SEARCH_CATEGORY.STATE:
          this.buildings = this.buildingsInfo.filter(building => building.state === (<HTMLInputElement>event.target).value).map(building => building.building)
          this.cities = this.buildingsInfo.filter(building => building.state === (<HTMLInputElement>event.target).value).map(building => building.city)
          break;
        default:
          break;
      }
    } else {
      this.fillSearchFieldOptions()
    }
  }

  searchBuilding() {
    this.tableData = this.buildingsInfo
    Object.keys(this.searchForm.controls).forEach((element) => {
      if (this.searchForm.value[element].trim()) {
        this.tableData = this.tableData.filter(data => data[element as keyof Building] === this.searchForm.value[element])
        this.sortData()
      }
    });
  }

  reset() {
    this.searchForm.reset({
      building: '',
      city: '',
      state: ''
    })
    this.fillTable()
    this.sortData()
  }

  fillSearchFieldOptions() {
    this.buildings = this.getInfoByCategory(SEARCH_CATEGORY.BUILDING);
    this.cities = this.getInfoByCategory(SEARCH_CATEGORY.CITY);
    this.states = this.getInfoByCategory(SEARCH_CATEGORY.STATE);
  }

  giveRating(event: Event, building: Building) {
    building.rating = parseInt((<HTMLSelectElement>event.target).value)
    localStorage.setItem('places', JSON.stringify(this.buildingsInfo))
  }

  sortData() {
    localStorage.setItem('sort', this.sort)
    switch (this.sort) {
      case 'asc':
        this.tableData = [...this.tableData].sort((a, b) => a.rating - b.rating)
        break;
      case 'desc':
        this.tableData = [...this.tableData].sort((a, b) => b.rating - a.rating)
        break;
      default:
        // this.tableData = this.buildingsInfo
        break;
    }
  }

}

enum SEARCH_CATEGORY {
  BUILDING = 'building',
  CITY = 'city',
  STATE = 'state'
}
