import { environment } from "./environments/environment";

export class Configuration {
    public static REMOTE_API_URL = environment.URL_END_POINT
}